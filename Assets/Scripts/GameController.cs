﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using Newtonsoft.Json;
using System.IO;

public class GameController : MonoBehaviour
{
    [SerializeField] private string fileName = "JsonChallenge.json";
    [SerializeField] private Text txtTitle;

    [Header("Data")]
    [SerializeField] private Transform dataParent;
    [SerializeField] private DataItem dataItemPrefab;
    [SerializeField] private Text dataTextPrefab;
    [SerializeField] private Text headerTextPrefab;

    private WatchFile watchFile;
    private bool jsonUpdated = false;

    void Start()
    {
        watchFile = new WatchFile();
        watchFile.CreateFileWatcher(Application.streamingAssetsPath, fileName, () => jsonUpdated = true);
        ReadJson();
    }

    private void Update()
    {
        if (jsonUpdated)
        {
            jsonUpdated = false;
            ReadJson();
        }
    }

    void ReadJson()
    {
        string path = $"{Application.streamingAssetsPath}/{fileName}";
        StreamReader sr = new StreamReader(path);
        string json = sr.ReadToEnd();
        sr.Close();

        FillScreen(json);
    }

    void FillScreen(string json)
    {
        Model model = JsonConvert.DeserializeObject<Model>(json);

        //Clear screen
        foreach (Transform item in dataParent)
        {
            Destroy(item.gameObject);
        }

        //Set title
        txtTitle.text = model.Title;

        //Set header
        DataItem header = Instantiate(dataItemPrefab, dataParent);
        header.Set(headerTextPrefab, model.ColumnHeaders);

        //Set data
        foreach (var item in model.Data)
        {
            DataItem dataItem = Instantiate(dataItemPrefab, dataParent);
            dataItem.Set(dataTextPrefab, model.ColumnHeaders, item);
        }
    }
}
