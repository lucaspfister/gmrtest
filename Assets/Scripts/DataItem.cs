﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DataItem : MonoBehaviour
{
    public void Set(Text textPrefab, string[] header, Dictionary<string, string> data = null)
    {
        foreach (string item in header)
        {
            Text txt = Instantiate(textPrefab, transform);

            //Check if is header line
            if (data == null)
            {
                txt.text = item;
                continue;
            }

            //Normal line
            txt.text = data.ContainsKey(item) ? data[item] : string.Empty;
        }
    }
}
