﻿using System;
using System.Collections.Generic;

[Serializable]
public class Model
{
    public string Title;
    public string[] ColumnHeaders;
    public Dictionary<string, string>[] Data;
}
