﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class WatchFile
{
    Action Callback;

    public void CreateFileWatcher(string path, string filter, Action callback)
    {
        Callback = callback;

        FileSystemWatcher watcher = new FileSystemWatcher();
        watcher.Path = path;
        watcher.NotifyFilter = NotifyFilters.LastWrite;
        watcher.Filter = filter;

        //Add event handlers
        watcher.Changed += new FileSystemEventHandler(OnChanged);

        //Begin watching
        watcher.EnableRaisingEvents = true;
    }

    private void OnChanged(object source, FileSystemEventArgs e)
    {
        Callback();
    }
}
